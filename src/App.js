import React from "react";
import { Route, Routes, Link } from "react-router-dom";
import MainPage from "./components/MainPage/MainPage";
import FormPage from "./components/FormPage/FormPage";
import "./App.css";

function App() {
  return (
    <>
      <div className="navigationBar">
        <div className="linksContainer">
          <Link to="/">Main page</Link>
          <Link to="/form">Form</Link>
        </div>

      </div>
      <Routes>
        <Route path="/" element={<MainPage/>} />
        <Route path="form" element={<FormPage/>} />
      </Routes>
    </>
  );
}

export default App;

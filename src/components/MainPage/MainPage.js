import React from "react";
import { useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import "./MainPage.css";

const MainPage = () => {
  const navigate = useNavigate();

  return (
    <div className="mainBtn">
      <Button onClick={() => {navigate("/form")}} variant="contained" size="large">FORM</Button>
    </div>
  );
};

export default MainPage;

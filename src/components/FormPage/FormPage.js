import React, { useState, useMemo } from "react";
import TextField from "@mui/material/TextField";
import Checkbox from "@mui/material/Checkbox";
import Button from "@mui/material/Button";
import "./FormPage.css";

const FormPage = () => {
  const [countField, setCountField] = useState(0);
  const [countTextArea, setCountTextArea] = useState(0);
  const [countCheckbox, setCountCheckbox] = useState(0);
  const [totalCount, setTotalCount] = useState({
    totalInput: 0,
    totalTextArea: 0,
    totalCheckbox: 0
  });

  const setValue = (setter, value) => {
    setter(Number(value))
  }

  const onClikBuild = () => {
    setTotalCount({
      totalInput: countField,
      totalTextArea: countTextArea,
      totalCheckbox: countCheckbox
    });
  };

  const label = { inputProps: { "aria-label": "Checkbox demo" } };

  // The first option

  const fieldInput = useMemo(() => {
    let elem = [];

    for (let i = 0; i < totalCount.totalInput; i++) {
      elem.push(<TextField sx={{margin: "10px 10px 0 0"}} label="Text field" variant="outlined"/>);
    }
    return elem;
  }, [totalCount]);

  const textAreaInput = useMemo(() => {
    let elem = [];

    for (let i = 0; i < totalCount.totalTextArea; i++) {
      elem.push(<textarea className="newTextArea" placeholder="Text area"></textarea>);
    }
    return elem;
  }, [totalCount]);

  // The second option

  const checkboxInput = useMemo(() => Array(totalCount.totalCheckbox).fill(<Checkbox/>),
    [totalCount]);

  return (
    <div>
      <div className="formContainer">
        <div className="fieldsContainer">
          <diV className="fieldContainer">
            <TextField id="outlined-basic" label="Text field" variant="outlined" />
            <TextField  value={countField} onChange={(e) => setValue(setCountField, e.target.value)} id="outlined-number" label="Number" type="number"
                        InputLabelProps={{
                          shrink: true,
                        }}
            />
          </diV>
          <div className="textareaContainer">
            <textarea placeholder="Text area">Text area</textarea>
            <TextField  value={countTextArea} onChange={(e) => setValue(setCountTextArea, e.target.value)} id="outlined-number" label="Number" type="number"
                        InputLabelProps={{
                          shrink: true,
                        }}
            />
          </div>
          <div className="checkboxContainer">
            <Checkbox {...label} />
            <TextField  value={countCheckbox} onChange={(e) => setValue(setCountCheckbox, e.target.value)} id="outlined-number" label="Number" type="number"
                        InputLabelProps={{
                          shrink: true,
                        }}
            />
          </div>
        </div>
        <Button onClick={onClikBuild} className="buildBtn" variant="contained" size="large">Build</Button>
      </div>
      <div className="newForm">
        <div className="newFieldsContainer">
          {fieldInput}
        </div>
        <div className="newTextAreaContainer">
          {textAreaInput}
        </div>
        <div className="newCheckbox">
          {checkboxInput}
        </div>
      </div>
    </div>
  );
};

export default FormPage;
